// import Index from '../pages/Index.vue';
// import TeamPage from '../pages/team/TeamPage.vue';
// import UserPage from '../pages/user/UserPage.vue';
// import SearchPage from '../pages/SearchPage.vue';
// import SearchPageResult from '../pages/SearchPageResult.vue';
// import UserAddTagsPage from '../pages/user/UserAddTagsPage.vue';
// import UserDetailPage from '../pages/user/UserDetailPage.vue';
// import UserEditPage from '../pages/user/UserEditPage.vue';
// import UserLoginPage from '../pages/UserLoginPage.vue';
// import TeamAddPage from '../pages/team/TeamAddPage.vue';
// import TeamViewPage from '../pages/team/TeamViewPage.vue';
// import TeamEditPage from '../pages/team/TeamEditPage.vue';
// import UserCreateTeamPage from '../pages/user/UserCreateTeamPage.vue';
// import UserJoinTeamPage from '../pages/user/UserJoinTeamPage.vue';
// import SettingPage from '../pages/user/SettingPage.vue';
// import ChatPage from '../pages/chat/ChatPage.vue';
// import FoundPage from '../pages/found/foundPage.vue';

// 2. 定义一些路由,每个路由都需要映射到一个组件。
const routes = [
  { 
    path: '/', 
    redirect: '/index'
  },
  { 
    path: '/test', 
    meta:{
      title: '寻员',
      requireAuth: false // 在需要登录的路由的meta中添加响应的权限标识为true
    }, 
    component: () => import('../pages/Test.vue') 
  },
  { 
    path: '/index', 
    meta:{
      title: '学科竞赛-首页',
      requireAuth: false // 在需要登录的路由的meta中添加响应的权限标识为true
    }, 
    component: () => import('../pages/Index.vue') 
  },
  { 
    path: '/notice/view',
    meta:{
      title: '官方通知',
      requireAuth: false,
    }, 
    component: () => import('../pages/NoticeViewPage.vue') 
  },
  { 
    path: '/team',
    meta:{
      title: '队伍',
      requireAuth: false,
    }, 
    component: () => import('../pages/team/TeamPage.vue') 
  },
  { 
    path: '/team/add', 
    meta:{
      title: '创建队伍',
      requireAuth: true,
    }, 
    component: () => import('../pages/team/TeamAddPage.vue')
  },
  { path: '/team/view', 
    meta:{
      title: '队伍详情',
      requireAuth: false,
    }, 
    component: () => import('../pages/team/TeamViewPage.vue')
  },
  { path: '/team/edit', 
    meta:{
      title: '编辑队伍信息',
      requireAuth: true,
    }, 
    component: () => import('../pages/team/TeamEditPage.vue')
  },
  { path: '/user/login',
    meta:{
      title: '登录',
      requireAuth: false,
    }, 
    component: () => import('../pages/UserLoginPage.vue') 
  },
  { path: '/user/register',
    meta:{
      title: '注册',
      requireAuth: false,
    }, 
    component: () => import('../pages/UserRegisterPage.vue') 
  },
  { path: '/user', 
    meta:{
      title: '用户',
      requireAuth: true,
    }, 
    component: () => import('../pages/user/UserPage.vue') 
  },
  { path: '/user/detail', 
    meta:{
      title: '个人资料',
      requireAuth: true,
    }, 
    component: () => import('../pages/user/UserDetailPage.vue') 
  },
  { path: '/user/edit', 
    meta:{
      title: '编辑信息',
      requireAuth: true,
    }, 
    component: () => import('../pages/user/UserEditPage.vue') 
  },
  { path: '/user/addTags', 
    meta:{
      title: '添加标签',
      requireAuth: true,
    }, 
    component: () => import('../pages/user/UserAddTagsPage.vue')
  },
  { path: '/user/my/create', 
    meta:{
      title: '创建的队伍',
      requireAuth: true,
    }, 
    component: () => import('../pages/user/UserCreateTeamPage.vue') 
  },
  { path: '/user/my/join', 
    meta:{
      title: '加入的队伍',
      requireAuth: true,
    }, 
    component: () => import('../pages/user/UserJoinTeamPage.vue')
  },
  { path: '/user/post/list', 
    meta:{
      title: '我的帖子',
      requireAuth: true,
    }, 
    component: () => import('../pages/user/UserCreatePostPage.vue')
  },
  { path: '/user/setting', 
    meta:{
      title: '设置',
      requireAuth: true,
    }, 
    component: () => import('../pages/user/SettingPage.vue')
  },
  { path: '/search/user', 
    tmeta:{
      title: '搜索',
      requireAuth: true,
    }, 
    component: () => import('../pages/SearchUserPage.vue')
  },
  { path: '/user/list', 
    meta:{
      title: '用户列表',
      requireAuth: true,
    }, 
    component: () => import('../pages/SearchUserResultPage.vue')
  },
  { path: '/chat', 
    meta:{
      title: '聊天',
      requireAuth: true,
    }, 
    component: () => import('../pages/chat/ChatPage.vue')
  },
  { path: '/chatPerson', 
    meta:{
      title: '聊天框',
      requireAuth: true,
    }, 
    component: () => import('../pages/chat/ChatPersonPage.vue')
  },
  { path: '/chatMessages', 
    meta:{
      title: '聊天信息',
      requireAuth: true,
    }, 
    component: () => import('../pages/chat/ChatMessages.vue')
  },
  { path: '/found', 
    meta:{
      title: '发现',
      requireAuth: false,
    }, 
    component: () => import('../pages/found/no-FoundPage.vue')
  },
  { path: '/found/search', 
    meta:{
      title: '帖文搜索',
      requireAuth: true,
    }, 
    component: () => import('../pages/found/PostSearchPage.vue')
  },
  { path: '/found/add', 
    meta:{
      title: '创建帖文',
      requireAuth: true,
    }, 
    component: () => import('../pages/found/PostAddPage.vue')
  },
  { path: '/found/update', 
    meta:{
      title: '',
      requireAuth: true,
    }, 
    component: () => import('../pages/found/PostUpdatePage.vue')
  },
  { path: '/found/view', 
    meta:{
      title: '',
      requireAuth: true,
    }, 
    component: () => import('../pages/found/PostViewPage.vue')
  },
  { path: '/found/report', 
    meta:{
      title: '',
      requireAuth: true,
    }, 
    component: () => import('../pages/report/ReportAddPage.vue')
  },
  { path: '/user/report/list', 
    meta:{
      title: '我的反馈',
      requireAuth: true,
    }, 
    component: () => import('../pages/report/MyReportListPage.vue')
  },
  
]

export default routes;
