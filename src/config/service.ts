import axios, {
  AxiosError,
  AxiosInstance,
  AxiosRequestConfig,
  AxiosRequestHeaders,
  AxiosResponse
} from "axios";

//不同环境自动区分baseUrl
// console.log(import.meta.env.VITE_BASE_API);
let BASE_URL = '';
if(import.meta.env.MODE === 'development') {
  BASE_URL = '/api';
}else if(import.meta.env.MODE === 'production') {
  BASE_URL = 'http://pm-backend.dengxian.space:8080/api';
}

// 创建axios实例
const service: AxiosInstance = axios.create({
    // baseURL: '/api',
  baseURL: BASE_URL,
});
service.defaults.withCredentials = true;

// 添加请求拦截器
service.interceptors.request.use(function (config) {
  // console.log("发个请求")
  // 在发送请求之前做些什么
  return config;
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error);
});

// 添加响应拦截器
service.interceptors.response.use(function (response) {
  // console.log("收个请求")
  // 未登录则跳转到登录页
  if (response.data.code === 40100) {
      const redirectUrl = window.location.href;
      window.location.href = `/user/login?redirect=${redirectUrl}`;
  }
  // 对响应数据做点什么
  return response.data;
}, function (error) {
  // 对响应错误做点什么

  return Promise.reject(error);
});

export { service }
