import {UserType} from "./user";

/**
 * 队伍类别
 */
export type TeamType = {
    id: number;
    //队长
    userId: number;
    name: string;
    avatarUrl?: string;
    description: string;
    expireTime?: Date;
    maxNum: number;
    password?: string,
    status: number;
    createTime: Date;
    updateTime: Date;
    createUser?: UserType;
    //加入队伍的数量
    hasJoinNum?: number;
    hasJoin: boolean;
};
