import { createApp } from 'vue'
// import './style.css'
import App from './App.vue'
import '@/assets/fonts/iconfont/iconfont.css'
import { NavBar } from 'vant';
import { Tabbar, TabbarItem } from 'vant';
import * as VueRouter from 'vue-router';
import routes from './config/route';
import { Tab, Tabs } from 'vant';
import { Icon } from 'vant';
import 'vant/es/dialog/style';
import { Field, CellGroup } from 'vant';
import { Cell } from 'vant';
import { Skeleton } from 'vant';
import { Card } from 'vant';
import { Divider } from 'vant';
import { Overlay } from 'vant';
import { PickerGroup } from 'vant';
import { Uploader } from 'vant';
import { Dialog } from 'vant';
import { Sticky } from 'vant';
import { Col, Row } from 'vant';
import { Swipe, SwipeItem } from 'vant';
import { Image as VanImage } from 'vant';
import { Search } from 'vant';
import { RadioGroup, Radio } from 'vant';
import { Popover } from 'vant';
// import { createPinia} from 'pinia';

// import store, { key } from './store'

import 'vant/es/toast/style';
// 引入状态管理
import { setupStore } from '@/store';

// createApp(App).mount('#app')
const app = createApp(App);

setupStore(app);
app.use(NavBar);
app.use(Tabbar);
app.use(TabbarItem);

// 创建路由实例并传递 `routes` 配置
const router = VueRouter.createRouter({
  //内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
  history: VueRouter.createWebHistory(),
  routes, // `routes: routes` 的缩写
})
router.afterEach((to, from) => {
  document.title = to.meta.title ? to.meta.title: '竞赛';//在全局后置守卫中获取路由元信息设置title
  
})

app.use(router);
app.use(Tab);
app.use(Tabs);
app.use(Icon);
app.use(Field);
app.use(CellGroup);
app.use(Cell);
app.use(Skeleton);
app.use(Card);
app.use(Divider);
app.use(Overlay);
app.use(PickerGroup);
app.use(Uploader);
app.use(Dialog);
app.use(Sticky);
app.use(Col);
app.use(Row);
app.use(Swipe);
app.use(SwipeItem);
app.use(VanImage);
app.use(Search);
app.use(Radio);
app.use(RadioGroup);
app.use(Popover);
// app.use(createPinia());
// app.use(store,key);

app.mount('#app');