import {service} from "@/config/service";

/** findAllInit GET /api/im/init/${param0} */
export async function findAllInitUsingGET(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.findAllInitUsingGETParams,
  options?: { [key: string]: any },
) {
  const { limit: param0, ...queryParams } = params;
  return service(`/im/init/${param0}`, {
    method: 'GET',
    params: { ...queryParams },
    ...(options || {}),
  });
}