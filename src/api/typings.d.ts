declare namespace API {
  type findAllInitUsingGETParams = {
    /** limit */
    limit: number;
  };

  type Im = {
    avatarUrl?: string;
    createTime?: string;
    id: number;
    img?: string;
    profile?: string;
    text?: string;
    uid?: number;
    toId?: number;
    username?: string;
  };

  type UserLoginRequest = {
    userAccount?: string;
    userPassword?: string;
  };
  
  type User = {
    avatarUrl?: string;
    contactInfo?: string;
    createTime?: string;
    gender?: number;
    id: number;
    isDelete?: number;
    phone?: string;
    planetCode?: string;
    profile?: string;
    tags?: string;
    updateTime?: string;
    userAccount?: string;
    userPassword?: string;
    userRole?: number;
    userStatus?: number;
    username?: string;
  };

 /**
 * 队伍类别
 */
 type Team = {
  id: number;
  //队长
  userId: number;
  name: string;
  avatarUrl?: string;
  description: string;
  expireTime?: Date;
  maxNum: number;
  password?: string,
  status: number;
  createTime: Date;
  updateTime: Date;
  createUser?: UserType;
  //加入队伍的数量
  hasJoinNum?: number;
  hasJoin: boolean;
};

  type Post = {
    id: int;
    title: string, //标题
    age?: int;
    gender?: int;
    education?: string;
    place?: string;
    job?: string;
    contact?: string;
    loveExp?: string;
    content: string;  //内容
    photo?: string;
    reviewStatus: int; //审核状态
    reviewMessage?: string; // 审核信息
    viewNum: int; // 浏览数
    thumbNum: int; // 点赞数
    userId: int; //创建用户id
    createTime: date;
    updateTime: date;
  }
  type Repost = {
    id: int;
    content: string;  //内容
    reportedId: int //被举报对象id
    reportedUserId: int //被举报用户id
    status: int; //审核状态
    userId: int; //创建用户id
    createTime: date;
    updateTime: date;
  }

  type Notice = {
    id: int;
    title: string, //标题
    content: string;  //内容
    // photo?: string;
    // viewNum: int; // 浏览数
    // thumbNum: int; // 点赞数
    userId: int; //创建用户id
    createTime: date;
    // updateTime: date;
  }
}